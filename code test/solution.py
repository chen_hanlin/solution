################################################################################
##      Written by: HANLIN CHEN
################################################################################

## Some import
import csv
import sys
import argparse
## Some constants
mini_name_size = 4 #minimum name size
name_warning = 'Name cannot be less than FOUR characters'
state_warning = 'The code denoting state MUST be in the file states.csv'
salary_warning = 'The salary must be an integer and not a float'
postcode_warning = 'The postcode must exist'

# Setting Flags
parser = argparse.ArgumentParser(description='Filter unwanted information')
parser.add_argument('-name', action='store_true',
                    help='Display Name warning, combined with -detail')
parser.add_argument('-salary', action='store_true',
                    help='Display salary warning, combined with -detail')
parser.add_argument('-state', action='store_true',
                    help='Display state warning, combined with -detail')
parser.add_argument('-postcode', action='store_true',
                    help='Display postcode errors, combined with -detail')
parser.add_argument('-detail', action='store_true',
                    help='View detailed output')
args = parser.parse_args()

## Reading file
file_data1 = 'data1.csv'
file_data2 = 'data2.csv'
file_states = 'states.csv'

data1 = open(file_data1)
data2 = open(file_data2)
states = open(file_states)

## Return a list of defined states
def get_states_array(state_file):
    states_array = []
    for row in csv.reader(states):
        if not row:
            continue
        if row[0] not in states_array:
            states_array.append(row[0])
    return states_array

# Print each element in a list
def print_list(l):
    for element in l:
        print(element)

# Get the index of a predefined string in a given list
# Return NULL if not found
def get_index(row, string):
    i = 0
    for element in row:
        if element == string:
            return i
        i += 1
    return None

# The main function
def csv_check(file, state_file):
    # Lists of warning/error
    name_errors = []
    state_errors = []
    salary_errors = []
    postcode_errors = []
    # Get the array of defined states from the given csv file
    states_array = get_states_array(state_file)
    
    print('Checking file: ' + file.name + '...\n')
    
    line = 0
    for row in csv.reader(file):
        if line == 0:
            ## If first line, identify the meaning of each column
            name_index = get_index(row, 'Name')
            state_index = get_index(row, 'State')
            salary_index = get_index(row, 'Salary')
            dob_index = get_index(row, 'Date of birth')
            postcode_index = get_index(row, 'Postcode')
        else:
            ## From second line, get and store each entry
            name = row[name_index]
            state = row[state_index]
            salary = row[salary_index]
            dob = row[dob_index]
            postcode = row[postcode_index]
            # Name check, save warning message if there is any
            if len(name) < mini_name_size:
                name_errors.append('At line: ' + str(line) + ', Name: ' +
                                    name + ', ' + name_warning)
            # State check, save warning message if there is any
            if state not in states_array:
                state_errors.append('At line: ' + str(line) + ', State: ' +
                                    state + ', ' + state_warning)
            # Salary check, save warning message if there is any
            if not salary.isdigit():
                salary_errors.append('At line: ' + str(line) + ', Salary: ' +
                                     salary + ', ' + salary_warning)
            # Postcode check, save error message if there is any
            if postcode == '':
                postcode_errors.append('At line: ' + str(line) + ', 
                	Postcode error: ' + name + ', ' + postcode_warning)
        line += 1
    ## Print the list of required warning/error
    if args.detail:
        ## Print name warning if the user requires
        if args.name:
            print_list(name_errors)
        ## Print state warning if the user requires
        if args.state:
            print_list(state_errors)
        ## Print salary warning if the user requires
        if args.salary:
            print_list(salary_errors)
        ## Print postcode error if the user requires
        if args.postcode:
            print_list(postcode_errors)
    ## By default, count the number of warning/error and display on the screen
    else:
        ## Warning user if forget to put -detail
        if args.name or args.state or args.salary or args.postcode:
            print("(If require detail, use argument '-detail', 
            	    or to get help use '-h')")
        else:
            print("If require more detail, try argument '-h'")
        warning_count = len(name_errors) + len(state_errors) + 
        len(salary_errors)
        if warning_count > 0:
            print(str(warning_count) +' warnings found')
        if len(postcode_errors) > 0:
            print(str(len(postcode_errors)) +' errors found')
        
    print('\nEnd of checking file: ' + file.name)

    ## Some version of python might quite the app directly before users being 
    ## able to read the output
    ## Use this dummy input() function to prevent that
    input("press ENTER to continue;\n")
    print('\n')

## The actually run
csv_check(data1, states)
csv_check(data2, states)

## Closing files
data1.close()
data2.close()
states.close()
